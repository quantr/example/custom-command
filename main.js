#!/usr/bin/env node
const program = require('commander');

  program
    .command('export-list <username>')
    .description('Export a SharePoint list to csv.')
    .action(function(name){
      const readline = require('readline').createInterface({
        input: process.stdin,
        output: process.stdout
      })
      
      var stdin = process.openStdin();
      process.stdin.on("data", function(char) {
        char = char + "";
        switch (char) {
          case "\n":
          case "\r":
          case "\u0004":
            stdin.pause();
          break;
          default:
            process.stdout.write("\033[2K\033[200D" + "Password: " + Array(readline.line.length+1).join("*"));
          break;
        }
      });
      
      readline.question("Password: ", function(pw) {
        readline.history = readline.history.slice(1);
        const cp = require('child_process')
        var child = cp.exec(`o365 login --authType password --userName ${name} --password ${pw}`, //-o json
       
          (error, stdout, stderr) => {
            if (error !== null) {
              console.log(`exec error: ${error}`);
              readline.close()
            }
            else{
              cp.fork(__dirname +'/export.js')
            }
          }
        )
      });
    });
  program.parse(process.argv);

      




